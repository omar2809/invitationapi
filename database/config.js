const mongoose = require('mongoose');
const {DB_CNN} = require('../keys.json')



const dbConnection = async() => {

    try {
        await mongoose.connect( DB_CNN , {
            useNewUrlParser: true, 
            useUnifiedTopology: true,
            useCreateIndex: true
        });

        console.log('DB Online');
        
    } catch (error) {
        console.log(error);
        throw new Error('Error a la hora de iniciar la BD ver logs');
    }


}


module.exports = {
    dbConnection
}