const { response } = require('express');
const { generarJWT } = require('../helpers/jwt');
const Invitation = require('../models/invitation');
const { compareAsc, subMonths, compareDesc, closestIndexTo, closestTo } = require('date-fns');




const getInvitation = async(req, res) => {

    try{


        const from = Number(req.query.from) || 0;

    
        const [ Invitations, total] = await Promise.all([
            Invitation
            .find({}, 'mobileNumber name source business language timeToCall typeOfReferral status dateCreation')
            .skip(from)
            .limit(5),
    
            Invitation.countDocuments()
        ]);
        
    
        res.json({
            ok: true,
            Invitations,
            total,
            
        })


    } catch (error) {
        console.log(error)
        res.status(500).json({
            ok: false,
            msg:'Error'
        });
      }

}

const postInvitation = async(req, res = response)=> {
   
    try {

        const { mobileNumber }  = req.body;  
        const   existPhone = await Invitation.findOne({ mobileNumber }).sort({dateCreation: -1})
    


    if(!existPhone) {

        const invitation =  new Invitation(req.body);
        //guardar usuario
        await invitation.save();
         //generar token
         const token = await generarJWT(invitation.uid);
      
          res.json({
           ok: true,
           invitation,
           token, 

        });
    }else{

        console.log(existPhone);

        const date1 =  existPhone.dateCreation;
        const date2 =  subMonths(new Date(), 8);
    
        const result = compareDesc(date1, date2);

        if( result === 1 ) {

           const invitation = new Invitation(req.body);            

            //guardar usuario
            await invitation.save();
            //generar toke
            const token = await generarJWT(invitation._id);
            
               res.json({
                ok: true,
                invitation,
                token,    
               
            });
    }
        else{
            res.status(404).json({
                ok: false,
                msg:'the phone was recently registered',
            });
          }
    }


    } catch (error) {
        console.log(error)
        res.status(500).json({
            ok: false,
            msg:'Error'
        });
      }
    }

     module.exports = {
        getInvitation,
        postInvitation

    }