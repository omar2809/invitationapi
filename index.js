const express = require('express');
const app = express();
const { dbConnection } = require('./database/config');
const cors = require('cors')
const path = require('path')
const morgan = require('morgan');

//socket io 
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);

  server.listen(3000, () => {
    console.log('listening on *:3000');
  });
  io.on('connection', (socket) => {
    console.log('a user connected'+ socket.id);
   
    socket.on('message', (data) => {
        socket.broadcast.emit('message', data);
    });
    });

 

//base de datos
dbConnection();

//cors
app.use(cors());

//directorio publico
app.use( express.static('public'));
app.use( express.json() );

//setting
app.set('view engine', 'ejs');
app.use(morgan('dev'));
app.use(express.urlencoded({extended: false}));

//rutas
app.use('/api/invitation', require('./routes/invitation'));




app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'public/index.html'));
});

