const {Schema, model} = require('mongoose');
const { format } =  require('date-fns');



const invitationSchema = Schema({

    mobileNumber: {
        type: String,
        require: true,
    },
    name: {
        type: String,
        require: true
    },
    source: {
        type: String,
        require: true,        
    },
    business: {
        type: String,
        require: true,
    },
    language: {
        type: String,
        require: true
    },
    timeToCall: {
        type: String,
        require: true
    },
    typeOfReferral: {
        type: String,
        require: true
    },
    status:{
        type: String,
        require: true,
        default: 'INVALID'
    },
    dateCreation:{
        type: Date,
        default: new Date(),
        require: true,
    },



});


invitationSchema.method('toJSON', function() {
    const {__v,_id, ...object} = this.toObject();
    Object.uid = _id;
    return object
});

module.exports = model('Invitation', invitationSchema);