const  jwt  = require("jsonwebtoken");
const { key } = require('../keys.json');


const validatorJWT = (req, res, next) => {

    //leer el token 
    const token = req.header('x-token');

    

    if (!token) {
        return res.status(401).json({
            ok:false,
            msg: 'no hay token en la peticion'
        })
    }
    
try {

    const { idtoken } = jwt.verify(token, key);
    req.uid = idtoken;
    
    next();
    
} catch (error) {
    return res.status(401).json({
        ok:false,
        msg: 'token invalido'
    })
    
}

}

module.exports = {
    validatorJWT,

}