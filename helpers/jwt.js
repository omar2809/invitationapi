const jwt = require('jsonwebtoken')
const { key } = require('../keys.json');


const generarJWT = () => {

    return new Promise( (resolve, reject) => {

        const payload = {
            idtoken:"99894561848914"
        };
    
        jwt.sign( payload, key , {
            expiresIn: '2h'
        },(err, token) => {
    
            if(err){
                console.log(err);
                reject('No se pudo generar el JWT')
            }else{
                resolve(token);
            }
    
        });
    })
 

  

}
module.exports = {
    generarJWT
}