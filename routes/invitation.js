const {Router} = require('express');
const {check} = require('express-validator');
const {validators} = require('../middlewares/validators');
const { getInvitation , postInvitation } = require('../controllers/invitation');
const { validatorJWT } = require('../middlewares/validator-jwt');
const router = Router();


router.get( '/',validatorJWT , getInvitation );


router.post( '/',
   [
       check('mobileNumber','el telefono tiene que ser valido').not().isEmpty(),
       check('name','el nombre es obligatorio').not().isEmpty(),
       check('source','el nombre es obligatorio').not().isEmpty(),
       check('business','el nombre es obligatorio').not().isEmpty(),
       check('language','el nombre es obligatorio').not().isEmpty(),
       check('timeToCall','el nombre es obligatorio').not().isEmpty(),
       check('typeOfReferral','el nombre es obligatorio').not().isEmpty(),
       validators,
       
   ],
   postInvitation
);

module.exports = router;
